import React from 'react';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css'

import './App.css';
import Navigation from './components/Navigation';
import PostList from './components/PostList';
import CreatePost from './components/CreatePost';
import UserSelect from './components/UserCreate';


function App() {
  return (
        <Router>
          <Navigation></Navigation>
          <Route path="/" exact component={PostList} />
          <Route path="/edit/:id" component={CreatePost} />
          <Route path="/create" component={CreatePost} />
          <Route path="/user" component={UserSelect} />
          
        </Router>
       );
}

export default App;
