import React, { Component } from 'react'
import axios from 'axios'

import Modalsize from './modal/Modalsize';

export default class CommentsPost extends Component {
    state={
        postId:0,
        comments:[]
    }
    async componentDidMount(){
        
        let postComments= await axios.get('https://jsonplaceholder.typicode.com/comments?postId='+this.props.postId);
        /*
        postComments.data.forEach(comment=>{
            console.log('Comm : '+JSON.stringify(comment.email));
        });
        /** */
        this.setState({
            postId:this.props.postId,
            comments:postComments.data
        })
    }
    render() {
        return (
            <div>
                
                <Modalsize content={this.state.comments}/>
            </div>
        )
    }
}
