import React, { Component } from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'
import CommentsPost from './CommentsPost'

export default class PostsList extends Component {

    state = {
        posts: []
    }

    componentDidMount() {
        this.getPosts();
    }

    getPosts = async () => {    
        const res = await axios.get('https://jsonplaceholder.typicode.com/posts')
        var postRender=[];
        res.data.map(async(post) => {
            let user = await axios.get('https://jsonplaceholder.typicode.com/users/'+post.userId);
            let postComments= await axios.get('https://jsonplaceholder.typicode.com/comments?postId='+post.id);
            let numComm=0;
            postComments.data.forEach(comment=>{
                numComm++
                //console.log('Comm : '+JSON.stringify(comment.email));
            });
            postRender.push({id:post.id,title:post.title,body:post.body,author:user.data.name,numberComments:numComm});
            this.setState({
                posts: postRender
            });
        })
    }

    deletePost = async (postId) => {
        await axios.delete('https://jsonplaceholder.typicode.com/posts/' + postId);
        this.getPosts();
    }

    render() {
        return (
            <div className="row"> 
                {
                    this.state.posts.map(post => (
                        
                        <div className="col-md-10 p-2" key={post.id}>
                            <div className="card">
                                <div className="card-header d-flex justify-content-between">
                                    <h5>{post.title}</h5>
                                    <Link to={"/edit/" + post.id} className="btn btn-secondary">
                                        <i className="material-icons">
                                            Edit</i>
                                    </Link>
                                </div>
                                <div className="card-body">
                                    <p>
                                        {post.body}
                                    </p>
                                    <p>
                                        Author: { post.author  }
                                    </p>
                                    <p>
                                        {"createdAt: "}
                                    </p>
                                </div>
                                <div className="card-footer">
                                    <div className="row">
                                        <div className="col-md-2">
                                            Comments :  - {post.numberComments} -
                                        </div>
                                        <div className="col-md-3">
                                            <CommentsPost postId={post.id}/> 
                                        </div>
                                    </div>
                                </div>
                                <div className="card-footer">
                                    <button className="btn btn-danger" onClick={() => this.deletePost(post.id)}>
                                        Delete
                                    </button>
                                </div>
                            </div>
                        </div>
                    ))
                }
            </div>
        )
    }
}