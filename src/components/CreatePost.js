import React, { Component } from 'react'
import axios from 'axios'

export default class CreatePost extends Component {

    state = {
        title: '',
        body: '',
    //    date: new Date(),
        userSelected: '',
        users: [],
        editing: false,
        id: ''
    }

    async componentDidMount() {
        const res = await axios.get('https://jsonplaceholder.typicode.com/users');
        if (res.data.length > 0) {
            this.setState({
                users: res.data.map(user => ({id:user.id,user:user.username})),
                userSelected: res.data[0].username.id
            })
        }
        if (this.props.match.params.id) {
        //    console.log(this.props.match.params.id)
            const res = await axios.get('https://jsonplaceholder.typicode.com/posts/' + this.props.match.params.id);
      //      console.log(res.data)
            this.setState({
                title: res.data.title,
                body: res.data.body,
           //     date: new Date(res.data.date),
                userSelected: res.data.userId,
                id: res.data.id,
                editing: true
            });
        }
    }

    onSubmit = async (e) => {
        e.preventDefault();
        if (this.state.editing) {
            const updatedNote = {
                title: this.state.title,
                body: this.state.body,
                userId: this.state.userSelected//,
   //             date: this.state.date
            };
            await axios.put('https://jsonplaceholder.typicode.com/posts/' + this.state.id, updatedNote);
        } else {
            const newNote = {
                title: this.state.title,
                body: this.state.body,
                userId: this.state.userSelected //,
  //              date: this.state.date
            };
            axios.post('https://jsonplaceholder.typicode.com/posts', newNote);
        }
        window.location.href = '/';

    }

    onInputChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    render() {
        return (
            <div className="col-md-6 offset-md-3">
                <div className="card card-body">
                    <h4>{this.state.editing?"Edit":"Create"} a post</h4>
                    <form onSubmit={this.onSubmit}>
                        {/* SELECT THE USER */}
                        <div className="form-group">
                            <select
                                className="form-control"
                                value={this.state.userSelected}
                                onChange={this.onInputChange}
                                name="userSelected"
                                required>
                                {
                                    this.state.users.map(user => (
                                        <option key={user.id} value={user.id}>
                                            {user.user}
                                        </option>
                                    ))
                                }
                            </select>
                        </div>
                        {/* Post Title */}
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Title"
                                onChange={this.onInputChange}
                                name="title"
                                value={this.state.title}
                                required />
                        </div>
                        {/* Post body */}
                        <div className="form-group">
                            <textarea
                                type="text"
                                className="form-control"
                                placeholder="Body"
                                name="body"
                                onChange={this.onInputChange}
                                value={this.state.body}
                                required>
                            </textarea>
                        </div>
                        
                        <button className="btn btn-primary">
                        {this.state.editing?"Update":"Save"} <i className="material-icons">
                                
</i>
                        </button>
                    </form>
                </div>
            </div>
        )
    }
}