import React,{useState}  from 'react'
import { Button,Modal, ButtonToolbar } from 'react-bootstrap';

export default function Modalsize(props) {
    const [smShow, setSmShow] = useState(false);
    const [lgShow, setLgShow] = useState(false);
  
    return (
      <ButtonToolbar>
        <Button onClick={() => setLgShow(true)}>view Comments</Button>
  
        
  
        <Modal
          size="lg"
          show={lgShow}
          onHide={() => setLgShow(false)}
          aria-labelledby="example-modal-sizes-title-lg"
        >
          <Modal.Header closeButton>
            <Modal.Title id="example-modal-sizes-title-lg">
              All the comments
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="container">
              {props.content.map((comentario)=>(
                <div className="card">
                <div className="row mb-3" >
                  <div className="col-md-12">
                    <b>user:</b> {comentario.email}
                  </div> 
                  <div className="col-md-12">
                      <b>{comentario.name}</b> 
                  </div>
                  <div className="col-md-12">
                      {comentario.body}
                  </div>
                  <hr />  
                </div>
                </div>
              ))}
            </div>
            
          </Modal.Body>
        </Modal>
      </ButtonToolbar>
    );
  }
  
  
  