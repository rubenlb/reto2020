import React, { Component } from 'react'
import axios from 'axios'

export default class UserCreate extends Component {

    state = {
        username: '',
        users: []
    }

    async componentDidMount() {
        this.getUsers();
    }

    getUsers = async () => {
        const res = await axios.get('https://jsonplaceholder.typicode.com/users');
        
        this.setState({
            users: res.data
        });
    }

    onChangeUsername = e => {
        this.setState({
            username: e.target.value
        })
    }

    onSubmit = async (e) => {
        e.preventDefault();
        await axios.post('https://jsonplaceholder.typicode.com/users', {
            username: this.state.username
        });
        this.setState({ username: '' });
        this.getUsers();
    }

    deleteUser = async (userId) => {
        const response = window.confirm('are you sure you want to delete it?');
        if (response) {
            await axios.delete('https://jsonplaceholder.typicode.com/users/' + userId);
            this.getUsers();
        }
    }

    render() {
        return (
            <div className="row">
                <div className="col-md-4">
                    <div className="card card-body">
                        <h3>Create New User</h3>
                        <form onSubmit={this.onSubmit}>
                            <div className="form-group">
                                <input
                                    className="form-control"
                                    value={this.state.username}
                                    type="text"
                                    onChange={this.onChangeUsername}
                                />
                            </div>
                            <button type="submit" className="btn btn-primary">
                                Save
                    </button>
                        </form>
                    </div>
                </div>
                <div className="col-md-8">
                    <ul className="list-group">
                        {
                            this.state.users.map(user => (
                                <li className="list-group-item list-group-item-action" key={user.id} onDoubleClick={() => this.deleteUser(user._id)}>
                                    <div className="row">
                                        <div className="col-md-3">
                                            <b>User: </b>{user.username}
                                        </div>
                                        <div className="col-md-4">
                                            <b>Name: </b> {user.name}
                                        </div>
                                        <div className="col-md-4">
                                            <b>Email: </b> {user.email}
                                        </div>
                                    </div>
                                      
                                </li>
                            ))
                        }
                    </ul>
                </div>
            </div>
        )
    }
}
